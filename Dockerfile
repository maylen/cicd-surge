
# *******************************************************
# docker build -t appui .
# docker run -d --name appui -p 8000:8000 appui
# docker exec <container-id> curl http://localhost:8000
# docker exec -it appui //bin//sh
# *******************************************************

FROM node:10-alpine as builder

COPY package.json package-lock.json ./

# also sort of caches node_modules
RUN npm install && mkdir /app-ui && mv ./node_modules ./app-ui

WORKDIR /app-ui
RUN ls

COPY . .

# in deploy-url give subdirectory if not root deployment
RUN npm run ng build -- --deploy-url=/ --prod


FROM nginx:alpine

#!/bin/sh

# copy local nginx.conf to container
COPY nginx/nginx.conf /etc/nginx/nginx.conf

# Remove default nginx index page
RUN rm -rf /usr/share/nginx/html/*

COPY --from=builder /app-ui/dist /usr/share/nginx/html
RUN cd /usr/share/nginx/html && echo "====== ARTIFACTS =======" && ls
EXPOSE 8095
ENTRYPOINT ["nginx", "-g", "daemon off;"]

