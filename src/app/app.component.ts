import { Component } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'cicd-test';
  showdata: any;

  constructor(private http: HttpClient) {}

  getData() {
    console.log('button toimii');
    this.http.get('https://www.tmispiritus.com/data')
      .subscribe((data: any) => {
        this.showdata = data;
    });

  }
}
