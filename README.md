# ****************************************************************************************
#  # https://stackoverflow.com/questions/39538464/how-to-include-custom-files-with-angular-cli-build

# (test change 157)
# Angular 9's Best Hidden Feature: Strict Template Checking - Find and report more errors 
# than ever with Angular 9's Ivy compiler, strict template checking.
# ****************************************************************************************
# see: https://auth0.com/blog/angular-9s-best-hidden-feature-strict-template-checking/
# see also: D:\Angular_2018\CICD-demo\cicd-test\tsconfig.json

# ****************************************************************************************
# To make ng e2e (end to end tests workin in Docker inb CI-CD)
# ****************************************************************************************
# see: how to set up chrome headless for docker in CI-CD
# https://forums.aws.amazon.com/thread.jspa?threadID=289974
# https://github.com/avatsaev/angular-chrome-headless-docker
# see lines 22-24 in D:\Angular_2018\CICD-demo\cicd-test\e2e\protractor.conf.js

# CicdTest

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.0.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
# CICD-Surge

PS D:\Angular_2018\CICD-demo\cicd-test> ng update @angular/cli
The installed local Angular CLI version is older than the latest stable version.
Installing a temporary version to perform the update.
Installing packages for tooling via npm.
Installed packages for tooling via npm.
Using package manager: 'npm'
Collecting installed dependencies...
Found 31 dependencies.
Fetching dependency metadata from registry...
    Updating package.json with dependency @angular/cli @ "10.0.5" (was "9.1.0")...
    Updating package.json with dependency @angular-devkit/build-angular @ "0.1000.5" (was "0.901.0")...
    Updating package.json with dependency @angular/common @ "10.0.8" (was "9.1.0")...
    Updating package.json with dependency @angular/forms @ "10.0.8" (was "9.1.0")...
    Updating package.json with dependency @angular/router @ "10.0.8" (was "9.1.0")...
    Updating package.json with dependency @angular/animations @ "10.0.8" (was "9.1.0")...
    Updating package.json with dependency @angular/compiler @ "10.0.8" (was "9.1.0")...
    Updating package.json with dependency @angular/core @ "10.0.8" (was "9.1.0")...
    Updating package.json with dependency @angular/platform-browser @ "10.0.8" (was "9.1.0")...
    Updating package.json with dependency @angular/platform-browser-dynamic @ "10.0.8" (was "9.1.0")...
    Updating package.json with dependency @angular/language-service @ "10.0.8" (was "9.1.0")...
    Updating package.json with dependency @angular/compiler-cli @ "10.0.8" (was "9.1.0")...
    Updating package.json with dependency typescript @ "3.9.7" (was "3.8.3")...
UPDATE package.json (1334 bytes)
√ Packages installed successfully.
** Executing migrations of package '@angular/cli' **

> Update Browserslist configuration file name to '.browserslistrc' from deprecated 'browserslist'.
RENAME browserslist => .browserslistrc
  Migration completed.

> Update tslint to version 6 and adjust rules to maintain existing behavior.
  Migration completed.

> Remove deprecated 'es5BrowserSupport' browser builder option.
  The inclusion for ES5 polyfills will be determined from the browsers listed in the browserslist configuration.
  Migration completed.

> Replace deprecated and removed 'styleext' and 'spec' Angular schematic options with 'style' and 'skipTests', respectively.
  Migration completed.

> Remove deprecated options from 'angular.json' that are no longer present in v10.
  Migration completed.

> Add "Solution Style" TypeScript configuration file support.
  This improves developer experience using editors powered by TypeScript’s language server.
  Read more about this here: https://v10.angular.io/guide/migration-solution-style-tsconfig
RENAME tsconfig.json => tsconfig.base.json
CREATE tsconfig.json (427 bytes)
UPDATE tsconfig.app.json (229 bytes)
UPDATE tsconfig.spec.json (275 bytes)
UPDATE e2e/tsconfig.json (219 bytes)
  Migration completed.

> Add the tslint deprecation rule to tslint JSON configuration files.
  Migration completed.

> Update library projects to use tslib version 2 as a direct dependency.
  Read more about this here: https://v10.angular.io/guide/migration-update-libraries-tslib
  Migration completed.

> Update workspace dependencies to match a new v10 project.
UPDATE package.json (1334 bytes)
√ Packages installed successfully.
  Migration completed.

> Update 'module' and 'target' TypeScript compiler options.
  Read more about this here: https://v10.angular.io/guide/migration-update-module-and-target-compiler-options
UPDATE tsconfig.base.json (541 bytes)
UPDATE e2e/tsconfig.json (222 bytes)
  Migration completed.


> Missing @Injectable and incomplete provider definition migration.
  As of Angular 9, enforcement of @Injectable decorators for DI is a bit stricter and incomplete provider definitions behave differently.
  Read more about this here: https://v9.angular.io/guide/migration-injectable
  Migration completed.

> ModuleWithProviders migration.
  As of Angular 10, the ModuleWithProviders type requires a generic.
  This migration adds the generic where it is missing.
  Read more about this here: https://v10.angular.io/guide/migration-module-with-providers
  Migration completed.

> Undecorated classes with Angular features migration.
  In version 10, classes that use Angular features and do not have an Angular decorator are no longer supported.
  Read more about this here: https://v10.angular.io/guide/migration-undecorated-classes
  Migration completed.

